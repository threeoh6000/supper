from tkinter import *
from tkinter import messagebox

wpassid = ""

def goat(ssid, password, code, file):
	d = file.get("1.0", "end-1c") + "wpa_supplicant.conf"
	y = "Is this location correct?",d
	answer = messagebox.askyesno(title="Supper", message="Is this location correct? {}".format(d))

	if answer:
		a = ssid.get("1.0", "end-1c")
		b = password.get("1.0", "end-1c")
		c = code.get("1.0", "end-1c")
		x = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\ncountry=" + c + "\nnetwork={\nssid=\"" + a + "\"\nscan_ssid=1\npsk=\"" + b + "\"\n}"

		if len(a) == 0:
			messagebox.showerror('Supper', 'SSID field not filled!')
			return
		if len(b) == 0:
			messagebox.showerror('Supper', 'Password field not filled!')
			return
		if len(c) == 0:
			messagebox.showerror('Supper', 'Country Code field not filled!')
			return

		f = open(d,"w")
		f.write(x)
		f.close()
		messagebox.showinfo(title='Supper', message='wpa_supplicant.conf generated successfully!')
		root.destroy()

root = Tk()
root.title("Supper")
root.minsize(600,500)
l = Label(text = "SSID (Network Name)")
ssid = Text(root, width = 20, height = 1)
lx = Label(text = "Password")
password = Text(root, width = 20, height = 3)
lb = Label(text = "Country code")
code = Text(root, width = 5, height = 1)
lc = Label(text = "Location of wpa_supplicant.conf")
file = Text(root, width = 25, height = 5)
b1 = Button(root, text = "Generate", command = lambda:goat(ssid, password, code, file))
l.pack()
ssid.pack()
lx.pack()
password.pack()
lb.pack()
code.pack()
lc.pack()
file.pack()
b1.pack()

root.mainloop()
